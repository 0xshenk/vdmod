﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Input;
using System.Windows.Forms;
using WindowsDesktop;
using GlobalHotKey;

namespace VDMod
{
    public partial class MainForm : Form
    {
        [DllImport("user32.dll", ExactSpelling = true)]
        static extern IntPtr GetForegroundWindow();

        private IList<VirtualDesktop> desktops;

        private IntPtr currentWindow;

        private readonly HotKeyManager _numHotkey;
        private readonly HotKeyManager _leftHotkey;
        private readonly HotKeyManager _rightHotkey;

        private bool closeToTray;
        

        public MainForm()
        {
            InitializeComponent();
            handleChangedNumber();

            closeToTray = true;

            VirtualDesktop.Created += VirtualDesktop_Added;
            VirtualDesktop.Destroyed += VirtualDesktop_Destroyed;

            this.FormClosing += MainForm_FormClosing;

            _numHotkey = new HotKeyManager();
            _numHotkey.KeyPressed += numHotkeyPressed;
            _leftHotkey = new HotKeyManager();
            _leftHotkey.KeyPressed += leftHotkeyPressed;
            _rightHotkey = new HotKeyManager();
            _rightHotkey.KeyPressed += rightHotkeyPressed;
        }

        private void numHotkeyPressed(object sender, KeyPressedEventArgs e)
        {
            var index = (int)e.HotKey.Key - (int)Key.D0 - 1;                                                                                                                                                                                                                                                                                                                
            var currentDesktop = getCurrentDesktopIndex();
            currentWindow = GetForegroundWindow();

            if (index == currentDesktop)
            {
                return;
            }
            
            if (index > desktops.Count - 1)
            {
                return;
            }

            if (currentWindow == IntPtr.Zero || currentWindow == null)
            {
                return;
            }

            VirtualDesktopHelper.MoveToDesktop(currentWindow, desktops.ElementAt(index));
        }

        private void leftHotkeyPressed(object sender, KeyPressedEventArgs e)
        {
            VirtualDesktop currentDesktop = VirtualDesktop.Current;
            VirtualDesktop left = currentDesktop.GetLeft();
            currentWindow = GetForegroundWindow();

            if (currentWindow == IntPtr.Zero || currentWindow == null)
            {
                return;
            }

            if (left != null)
            {
                VirtualDesktopHelper.MoveToDesktop(currentWindow, left);
            }
        }

        private void rightHotkeyPressed(object sender, KeyPressedEventArgs e)
        {
            VirtualDesktop currentDesktop = VirtualDesktop.Current;
            VirtualDesktop right = currentDesktop.GetRight();
            currentWindow = GetForegroundWindow();

            if (currentWindow == IntPtr.Zero || currentWindow == null)
            {
                return;
            }

            if (right != null)
            {
                VirtualDesktopHelper.MoveToDesktop(currentWindow, right);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.Visible = false;
            this.ShowInTaskbar = false;
            registerHotkeys();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Visible = false;
            this.ShowInTaskbar = false;
            notifyIcon1.BalloonTipTitle = "Still Running...";
            notifyIcon1.BalloonTipText = "Right-click on the tray icon to exit.";
            notifyIcon1.ShowBalloonTip(2000);
        }

        private void handleChangedNumber()
        {
            desktops = VirtualDesktop.GetDesktops();
        }

        private void VirtualDesktop_Added(object sender, VirtualDesktop e)
        {
            handleChangedNumber();
        }

        private void VirtualDesktop_Destroyed(object sender, VirtualDesktopDestroyEventArgs e)
        {
            handleChangedNumber();
        }

        private void registerHotkeys()
        {
            try
            {
                ModifierKeys modifiers = System.Windows.Input.ModifierKeys.Control | System.Windows.Input.ModifierKeys.Shift;

                _numHotkey.Register(Key.D1, modifiers);
                _numHotkey.Register(Key.D2, modifiers);
                _numHotkey.Register(Key.D3, modifiers);
                _numHotkey.Register(Key.D4, modifiers);
                _numHotkey.Register(Key.D5, modifiers);
                _numHotkey.Register(Key.D6, modifiers);
                _numHotkey.Register(Key.D7, modifiers);
                _numHotkey.Register(Key.D8, modifiers);
                _numHotkey.Register(Key.D9, modifiers);
                _numHotkey.Register(Key.D0, modifiers);

                _leftHotkey.Register(Key.Left, modifiers);
                _rightHotkey.Register(Key.Right, modifiers);
            }
            catch (Exception ex)
            {

            }
        }

        private int getCurrentDesktopIndex()
        {
            return desktops.IndexOf(VirtualDesktop.Current);
        }

        private void exitContextMenuItem_Click(object sender, EventArgs e)
        {
            _rightHotkey.Dispose();
            _leftHotkey.Dispose();
            _numHotkey.Dispose();

            closeToTray = false;

            this.Close();
        }
    }
}
